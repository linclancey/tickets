import Mock from 'mockjs';

Mock.setup({
    timeout: '400-2500'
});

Mock.mock(/tickets/, 'get', {
    success: true,
    "data|3-8": [
        {
            "id|+1": 1,
            "name|1": [
                "远程支持票",
                "abc票",
                "标准票",
                "@cname()票"
            ],
            "price": "@float(0.99, 999, 2, 2)",
            "description|1": [
                "可获得活动 PPT 等资料",
                "可获得品牌露出机会，详情请联系微信",
                "可获得活动现场参加活动的机会",
                "@csentence",
            ],
            "remain|1": [-1, 0, 2, 4, 10, 6],
        }
    ]
});

Mock.mock(/tickets/, 'post', {
    success: true,
});

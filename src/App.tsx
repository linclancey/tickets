import React, { useState, useEffect } from 'react';
import axios from 'axios';

import useForm from './useForm';
import Footer from './components/Footer';
import Purchaser from './components/Purchaser';
import { TicketList } from './components/Ticket';
import { ParticipantList } from './components/Participant';
import Modal from './components/Modal';

import './mock/tickets';
import './App.css';
import { setTimeout } from 'timers';

const regex = {
  phone: /^(?:(?:\+|00)86)?1\d{10}$/,
  idNumber: /^\d{6}(18|19|20)\d{2}(0\d|10|11|12)([0-2]\d|30|31)\d{3}(\d|X|x)$/,
  email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
};

const App: React.FC = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [isVisible, SetIsVisible] = useState(false);
  const [ticketsList, setTicketsList] = useState([]);
  const [data, setData] = useState({
    nums: 0,
    price: 0,
  });

  const {
    values,
    errors,
    isSubmitting,
    handleChange,
    handleSubmit,
  } = useForm(submit, validate);

  function submit(setS: any) {
    axios.post('/api/tickets', values)
      .then(res => {
        setS(false);
        SetIsVisible(true);

        setTimeout(() => {
          SetIsVisible(false);
        }, 500);
      });
  }

  function validate(values: any) {
    const {participants, purchaser} = values;
    const errors: any = {};

    const partKeys = Object.keys(participants);
    if (partKeys.length === 0) errors.partNums = 1;

    partKeys.map(key => {
      const part = participants[key];

      if (part.name.length < 2) {
        if (!errors.participantName) errors.participantName = [];
        errors.participantName.push(part.key);
      }

      if (!regex.idNumber.test(part.id)) {
        if (!errors.participantNumber) errors.participantNumber = [];
        errors.participantNumber.push(part.key);
      }

      if (['male', 'female', 'other', 'decline'].indexOf(part.sex) < 0) {
        if (!errors.participantSex) errors.participantSex = [];
        errors.participantSex.push(part.key);
      }
    });

    if (!regex.phone.test(purchaser.phone)) {
      errors.purchaserPhone = 1;
    }

    if (!regex.email.test(purchaser.email)) {
      errors.purchaserEmail = 1;
    }

console.log(errors);
    return errors;
  }

  useEffect(() => {
    axios('/api/tickets')
      .then(res => {
        setTicketsList(res.data.data);
        setIsLoading(false);
      });
  }, []);

  useEffect(() => {
    let price = 0, nums = 0;
    for (const key in values.tickets) {
      const ticket = values.tickets[key];
      nums += ticket.total;
      price += ticket.total * ticket.price;
    }
    setData({nums, price});
  }, [values.tickets]);

  return (
    <div className="App">
      <form className="form" onSubmit={handleSubmit}>
        <div className="form-wrapper">
          <div className="tickets">
            <header className="header"><h2>选择票种</h2>{errors.partNums ? <span className="tickets-error">请选择票种</span> : ''}</header>
            <TicketList isLoading={isLoading} data={ticketsList} errors={errors} handleChange={handleChange} />
          </div>

          <Purchaser handleChange={handleChange} errors={errors} />
          <ParticipantList data={values.tickets} errors={errors} handleChange={handleChange} />
        </div>
        <Footer data={data} isSubmitting={isSubmitting} />
      </form>
      <Modal visible={isVisible} />
    </div>
  );
}

export default App;

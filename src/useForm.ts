import { useState, useEffect } from 'react';

const useForm = (callback: any, validate: any) => {
  const defaultState : any = {
    tickets: {},
    purchaser: {},
    participants: {},
  };

  const defaultError: any = {};
  const [errors, setErrors] = useState(defaultError);
  const [values, setValues] = useState(defaultState);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback(setIsSubmitting);
    } else {
      setIsSubmitting(false);
    }
  }, [errors]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    setIsSubmitting(true);
    setErrors(validate(values));
  }

  const handleChange = (name: string, value: number) => {
    setValues((values: any) => ({ ...values, [name]: value}));
  }

  return {
    handleChange,
    handleSubmit,
    isSubmitting,
    values,
    errors,
  };
};

export default useForm;
import React, {useState, useEffect} from 'react';
import Input from '../Input';

interface PurchaserProps {
  errors: any;
  handleChange: any;
}

const Purchaser: React.FC<PurchaserProps> = props => {
  const [purchaser, setPurchaser] = useState({
    email: '',
    phone: '',
  });

  const handleChange = (e: any) => {
    e.persist();
    setPurchaser(values => ({ ...values, [e.target.name]: e.target.value }));
  }

  useEffect(() => {
    props.handleChange('purchaser', purchaser);
  }, [purchaser]);

  return <div className="purchaser-info">
          <header className="header"><h2>购票人信息</h2></header>
          <div className="body form-body">
            <Input name="phone" label="手机号" error={props.errors.purchaserPhone || 0} errorMsg="请输入正确且有效的手机号" placeholder="请输入您的手机号，将用于接受出票人短信" onChange={handleChange}></Input>
            <Input name="email" label="电子邮箱" error={props.errors.purchaserEmail || 0} errorMsg="请输入正确且有效的电子邮箱"  placeholder="请输入您的电子邮箱，将用于接受出票邮件" onChange={handleChange}></Input>
          </div>
        </div>;
}

export default Purchaser;
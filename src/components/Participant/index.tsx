import React, {useState, useEffect} from 'react';

import Input from '../Input';
import { Radio, RadioGroup } from '../Radio';

import './index.css';

interface TicketProps {
  key: string;
  name: string;
  price: string;
  total: number;
}

interface ParticipantListProps {
  data: any;
  errors: any;
  handleChange: any;
}

export interface ParticipantProps {
  name: string;
  errors: Array<string>;
  data: TicketProps;
  handleChange: any;
}

const ParticipantList: React.FC<ParticipantListProps> = props => {
  const [list, set] = useState({});

  const handleChange = (k: string, v: number) => {
    set(list => ({...list, [k]: v}));
  };

  useEffect(() => {
    props.handleChange('participants', list);
  }, [list]);

  const render = (ticket: any) => {
    let parts: any = [];
    let j = 0;

    const {participantName, participantNumber, participantSex} = props.errors;

    Object.keys(ticket).map(key => {
      for (let i = 0; i < ticket[key].total; i++) {
        const errors = [];

        if (participantName && participantName.indexOf(j.toString()) > -1) errors.push('name');
        if (participantNumber && participantNumber.indexOf(j.toString()) > -1) errors.push('number');
        if (participantSex && participantSex.indexOf(j.toString()) > -1) errors.push('sex');

        parts.push(<div key={j} className="participator-info">
          <header className="header"><h2>参与者信息（“{ticket[key].name}”第 {i+1} 位）</h2></header>
          <div className="body form-body"><Participant key={j} errors={errors} name={j.toString()} data={ticket[key]} handleChange={handleChange} /></div>
        </div>);
        j++;
      }
    })

    return parts;
  }

  return <div>{render(props.data)}</div>;
}

const Participant: React.FC<ParticipantProps> = props => {
  const [participant, setParticipant] = useState({
    key: props.name,
    name: '',
    phone: 0,
  });

  const handleChange = (e: any) => {
    e.persist();
    const name = e.target.name.slice(1, -1).split('-')[1];
    setParticipant(values => ({ ...values, [name]: e.target.value }));
  }

  useEffect(() => {
    props.handleChange(props.name, participant);
  }, [participant]);

return <div className={props.errors.length > 0 ? 'parti-error' : ''}>
    <Input name={`parti-name${props.name}`} error={props.errors.indexOf('name') > -1} errorMsg="请输入您的姓名" label="姓名" placeholder="请输入您的真实姓名" onChange={handleChange}></Input>
    <Input name={`parti-id${props.name}`} error={props.errors.indexOf('number') > -1} errorMsg="请输入正确且有效的身份证号码" label="身份证号码" placeholder="由于现场安保需要，请输入您的身份证号码" onChange={handleChange}></Input>
    <RadioGroup name={`parti-sex${props.name}`} error={props.errors.indexOf('sex') > -1} label="性别" onChange={handleChange}>
      <Radio value="male">男性</Radio>
      <Radio value="female">女性</Radio>
      <Radio value="other">其他</Radio>
      <Radio value="decline">不愿透露</Radio>
    </RadioGroup>
  </div>;
}

export {
  Participant,
  ParticipantList,
};
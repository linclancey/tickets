import React from 'react';

import Spin from '../Spin';

import './index.css';

interface DataProps {
  nums: number;
  price: number;
};

interface FooterProps {
  data: DataProps;
  isSubmitting: boolean;
};

const Footer: React.FC<FooterProps> = props => {
  return <footer className="footer">
    <div className="footer-left">
      <p>共 <span className="footer-left-color">{props.data.nums}</span> 张票</p>
      <p>合计 <span className="footer-left-color footer-left-size">{props.data.price.toFixed(2)}</span> 元</p>
    </div>
    <div className="footer-right">
      <button type="submit" className="footer-submit">{props.isSubmitting ? <div><Spin /> 支付中</div>:  '立即支付'}</button>
    </div>
  </footer>;
}

export default Footer;
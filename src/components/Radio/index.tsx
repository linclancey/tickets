import React from 'react';
import './index.css';

interface RadioGroupProps {
  error: boolean;
  label: string;
  name: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
}

interface RadioProps {
  value: string;
  children: React.ReactNode;
}

interface contextProps {
  name?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
}

const context: contextProps = {};

const RadioGroup: React.FC<RadioGroupProps> = props => {
  context.name = props.name || '';
  context.onChange = props.onChange;

  return <div className={`form-radio${props.error ? ' form-radio-error' : ''}`}>
    <div className="radiog-label">{props.label}</div>
    <div className="radiog-wrap">{props.children}</div>
    {props.error ? <div className="radiog-error">请正确选择性别</div> : ''}
  </div>;
}

const Radio: React.FC<RadioProps> = props => {
  return (<div className="radio-wrapper">
    <span className="radio">
      <input className="radio-input" type="radio" id={`${context.name}-${props.value}`} name={context.name} value={props.value} onChange={context.onChange} />
      <span className="radio-inner"></span>
    </span>
    <label className="radio-label" htmlFor={`${context.name}-${props.value}`}>{props.children}</label>
  </div>);
}

export { Radio, RadioGroup };

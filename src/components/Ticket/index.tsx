import React, {useState, useEffect} from 'react';
import Spin from '../Spin';

import './index.css';

export interface TicketProps {
  id: number;
  name: string;
  price: string;
  remain: number;
  description: string;
}

interface TicketListProps {
  data: Array<TicketProps>;
  isLoading: boolean;
  errors: Object;
  handleChange: any;
}

interface TicketWrapperProps {
  key?: number;
  name?: string;
  data: TicketProps;
  value?: number;
  handleChange: any;
}

const TicketList: React.FC<TicketListProps> = props => {
  const [ticketList, setTicketList] = useState({});

  const handleChange = (k: string, v: number) => {
    setTicketList(list => ({...list, [k]: v}));
  };

  useEffect(() => {
    props.handleChange('tickets', ticketList);
    props.handleChange('participants', {});
  }, [ticketList]);

  return <div className="body">
    {props.isLoading ? <div className="ticket-loading">
      <Spin /> 加载中
    </div> : props.data && props.data.map((ticket: TicketProps) => (
      <Ticket name={ticket.id.toString()} key={ticket.id} data={ticket} handleChange={handleChange}></Ticket>
    ))}
  </div>;
}

const Ticket: React.FC<TicketWrapperProps> = props => {
  const {price, name, description, remain} = props.data;
  const [nums, setNums] = useState(0);
  const [active, setActive] = useState([0, 0 === remain ? 0 : 1]);

  const set = (method: string) => {
    let num: number = +nums;

    num = method === 'reduce' ?
          num > 1 ? num - 1 : 0 :
          (remain < 0 || num < remain) ? num + 1 : remain;

    setNums(num);
  }

  useEffect(() => {
    let num = nums;
    setActive([
      num > 0 ? 1 : 0,
      (remain < 0 || remain - num > 0) ? 1 : 0,
    ]);

    props.handleChange(props.name, { name, price, total: +nums });
  }, [nums]);

  const handleChange = (e: any) => {
    let num = +e.target.value;

    if (num < 0) num = 0;
    if (num > remain && remain >= 0) num = remain;

    setNums(num);
  }

  return <div className="ticket">
    <div className="ticket-left">
      <h2>{name}</h2>
      <p>{description}</p>
      <footer>
        <span className="ticket-price"><i>{price}</i> 元</span>
        {remain >= 0 ? <span className="ticket-remain">{remain === 0 ? '售完' : `仅剩 ${remain} 张`}</span> : ''}
      </footer>
    </div>
    <div className="ticket-right">
      <div className="ticket-select">
        <div className={`ticket-reduce${active[0] ? ' is-active' : ''}`} onClick={() => set('reduce')}></div>
        <div className="ticket-nums"><input type="number" name={props.name} value={nums || '0'} onChange={handleChange}/></div>
        <div className={`ticket-add${active[1] ? ' is-active' : ''}`} onClick={() => set('add')}></div>
      </div>
    </div>
  </div>;
}

export {
  Ticket,
  TicketList,
};
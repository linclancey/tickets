import React from 'react';
import './index.css';

export interface InputProps {
  label?: string;
  name?: string;
  error?: any;
  errorMsg?: string;
  placeholder?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
}

const Input: React.FC<InputProps> = props => {
  return <div className={`form-input${props.error ? ' form-error' : ''}`}>
    <label className="form-input-label" title={props.label}>{props.label}</label>
    <input className="form-input-input"
      name={props.name}
      placeholder={props.placeholder}
      onChange={props.onChange}
    />
    <div className="form-input-error">{props.error ? props.errorMsg : ''}</div>
  </div>;
}

export default Input;